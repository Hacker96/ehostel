<!DOCTYPE html>
<html ng-app="app" ng-strict-di>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Room Dashboard | eHostel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Laravel Angular -->
    <meta name="theme-color" content="#0690B7">

    <link rel="manifest" href="manifest.json">

    <!--[if lte IE 10]>
    <script type="text/javascript">document.location.href = '/unsupported-browser'</script>
    <![endif]-->

    <style><?php require( public_path( "css/critical.css" ) ) ?></style>

    <link rel="stylesheet" href="{!! elixir('css/final.css') !!}">
    <!-- Laravel Angular - END -->

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="assets/adminLTE_2/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/adminLTE_2/bootstrap/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="assets/css/ionicons.min.css"> -->
    <!-- DataTables -->
    <link rel="stylesheet" href="assets/adminLTE_2/plugins/datatables/dataTables.bootstrap.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="assets/adminLTE_2/plugins/daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="assets/adminLTE_2/plugins/datepicker/datepicker3.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="assets/adminLTE_2/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="assets/adminLTE_2/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="assets/adminLTE_2/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="assets/adminLTE_2/plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="assets/adminLTE_2/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="assets/adminLTE_2/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="assets/css/my_ehostel.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">

    <div ui-view="layout"></div>

    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="{!! elixir('js/final.js') !!}" sync defer></script>

<!-- jQuery 2.2.3 -->
<script src="assets/adminLTE_2/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/adminLTE_2/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="assets/adminLTE_2/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="assets/adminLTE_2/plugins/input-mask/jquery.inputmask.js"></script>
<script src="assets/adminLTE_2/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="assets/adminLTE_2/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="assets/adminLTE_2/plugins/daterangepicker/moment.min.js"></script>
<script src="assets/adminLTE_2/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="assets/adminLTE_2/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="assets/adminLTE_2/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="assets/adminLTE_2/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- DataTables -->
<script src="assets/adminLTE_2/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/adminLTE_2/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="assets/adminLTE_2/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="assets/adminLTE_2/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="assets/adminLTE_2/plugins/fastclick/fastclick.js"></script>
<!-- Chart -->
<script src="assets/adminLTE_2/plugins/chartjs/Chart.min.js"></script>

</body>
</html>
