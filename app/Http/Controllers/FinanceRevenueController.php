<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\FinanceRevenue;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FinanceRevenueController extends Controller
{
    public function collect(Request $request) {

        $is_search = isset($request->is_search) ? $request->is_search : 0;
        $year = isset($request->search_year) ? $request->search_year : 0;
        $month = isset($request->search_month) ? $request->search_month : 0;
        $room = isset($request->search_room) ? $request->search_room : 0;
        $page_index = isset($request->search_page_index) ? $request->search_page_index : 1;
        $page_size = isset($request->search_page_size) ? $request->search_page_size : 10;

        $revenues = FinanceRevenue::select_revenue_search($year,$month,$room,$page_index,$page_size);

        foreach ($revenues as $value){
            $value->revenue_year_month = $this->getDateFormat($value->revenue_year_month);
        }

        $rooms = Room::select_for_search();
        $current_year = date("Y");

        $result = array(
            'rooms' => $is_search ? array() : $rooms,
            'years' => $is_search ? array() : array(
                0                => 'All',
                $current_year    => $current_year,
                $current_year -1 => $current_year - 1,
                $current_year -2 => $current_year -2,
                $current_year -3 => $current_year -3,
                $current_year -4 => $current_year -4
            ),
            'paging_info' => array(
                'total_row' => 30,
                'current_page' => 1,
                'row_number' => 10,
                'start-row' => 1,
                'end_row' => 10,
                'total_page' => 3
            ),
            'revenue_data' => $revenues,
        );


        return response()
            ->json($result);
    }
    // get Revenue by Id
    public function get($id) {

        $revenue = FinanceRevenue::where('revenue_id', $id)
            ->where('del_flg', 0)->first();

        if (empty($revenue)) {
            //Return 404
            return response()->error('Not found', 404);
        }

        $room = Room::where('room_id', $revenue -> room_id)
            ->where('del_flg', 0)->first();

        $result = array(
            'revenue' => $revenue,
            'room'    => $room,
        );

        return response()
            ->json($result);
    }

    public function getDateFormat($param){
        if(strlen($param) == 6){
            return substr($param,0,4).'/'.substr($param,4,2);
        }
        return '';
    }

}
