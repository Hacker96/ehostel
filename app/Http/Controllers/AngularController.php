<?php

namespace App\Http\Controllers;

class AngularController extends Controller
{
    public function serveApp()
    {
//        return view('index');
        return view('index_admin');
    }

    public function unsupported()
    {
        return view('unsupported_browser');
    }
}
