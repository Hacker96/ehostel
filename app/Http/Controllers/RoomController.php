<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Guest;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
//use Illuminate\Support\Facades\DB;

class RoomController extends Controller
{
    //

    public function getlist($room_id = null) {
        if($room_id == null) {
            return DB::table('room')->get();
        }
        else {
            return DB::table('room')->where('room_id', '=', $room_id)->get();
        }
    }
    public function collect(Request $request) {

        //Get list room
        $room_list = Room::collect();

        $result = [];
        foreach($room_list as $k=>$room) {

            if ( ! isset($result[$room->zone_id]) ) {
                $result[$room->zone_id] = array(
                    'zone_id' => $room->zone_id,
                    'zone_name' => $room->zone_name,
                    'zone_address' => $room->address,
                    'status' => 0,
                    'rooms' => array(
                        array(
                            'room_id' => $room->room_id,
                            'room_name' => $room->room_name,
                            'status' => ($k % 4)+1
                        )
                    ),
                );
            } else {
                $result[$room->zone_id]['rooms'][] = array(
                    'room_id' => $room->room_id,
                    'room_name' => $room->room_name,
                    'status' => ($k % 4) + 1
                );
            }
        }

        $rs = [];
        foreach($result as $v) {
            $rs[] = $v;
        }

        return response()
            ->json($rs);
    }
//
//    public function get(Request $request) {
//
//        $result = $request;
//
//        return response()
//            ->json($result);
//    }
//
    public function get($room_id) {

        /*--- Get config ---*/
        $config = config('constants.UPLOAD_FILE');
        $upload_path = $config['PATH'];
        $env = $config['ENV'];
        /*--- Get config - END ---*/

        $room_info = Room::find($room_id);

        if (empty($room_info)) {
            //Return 404
            return response()->error('Not found', 404);
        }

        //Get contract
        $contract_info = Contract::get_contract_for_room_detail($room_id);

        //Get guest list
        $guests = array();
        if (!empty($contract_info)) {
            $guests = Guest::get_guest_by_room($room_id);

            if (!empty($guests)) {
                $arr_temp = [];
                foreach($guests as $guest) {
                    if ($guest->avatar != NULL) {
                        $guest->avatar = $upload_path . $env . $config['ROOT_GUEST_AVATAR'] . $guest->avatar;
                    } else {
                        $guest->avatar = './assets/img/default-avatar.png';
                    }
                    $arr_temp[] = $guest;
                }
                $guests = $arr_temp;
            }
        }

        $result = array(
            'room_name' => $room_info->room_name,
            'room_price' => number_format(!empty($contract_info) ? $contract_info->room_price : $room_info->price),
            'room_status' => 1,
            'contract_id' => isset($contract_info->account_contract_id) ? $contract_info->account_contract_id : 0,
            'contract_by' => !empty($contract_info) ? $contract_info->first_name . ' ' . $contract_info->last_name : '',
            'contract_by_id' => !empty($contract_info) ? $contract_info->guest_id : 0,
            'contract_start_date' => !empty($contract_info) ? $contract_info->start_date : 0,
            'contract_end_date' => !empty($contract_info) ? $contract_info->end_date : 0,
            'guests' => $guests,
            'number_guest' => count($guests),
        );

        return response()
            ->json($result);
    }
}
