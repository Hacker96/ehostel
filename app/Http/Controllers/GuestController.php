<?php
/**
 * Created by PhpStorm.
 * User: Triet-Nguyen
 * Date: 9/19/2016
 * Time: 10:36 AM
 */

namespace App\Http\Controllers;

use App\CommonModel;
use App\FileMst;
use App\Room;
use App\Guest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;

class GuestController extends Controller
{

    public function getlist($room_id = null) {
        if($room_id == null) {
            return DB::table('guest')->get();
        }
        else {
            return DB::table('guest')->where('room_id', '=', $room_id)->get();
        }
    }
    public function collect(Request $request) {

        $is_search = isset($request->is_search) ? $request->is_search : 0;
        $name = isset($request->search_name) ? trim($request->search_name) : '';
        $room = isset($request->search_room) ? $request->search_room : 0;
        $gender = isset($request->search_gender) ? $request->search_gender : 0;
        $page_index = isset($request->search_page_index) ? $request->search_page_index : 1;
        $page_size = isset($request->search_page_size) ? $request->search_page_size : 10;

        $guests = Guest::select_guest_search($name,$room,$gender,$page_index,$page_size);

        $rooms = Room::select_for_search();

        $room_all = new \stdClass();
        $room_all->room_id = 0;
        $room_all->room_name = '--- All ---';
        array_unshift($rooms, $room_all);

        $genders = array(
            0   =>  'All',
            1   =>  'Male',
            2   =>  'Female',
            3   =>  'Other',
        );

        $result = array(
            'rooms' => $is_search ? array() : $rooms,
            'genders' => $is_search ? array() : $genders,
            'paging_info' => array(
                'total_row' => 30,
                'current_page' => 1,
                'row_number' => 10,
                'start-row' => 1,
                'end_row' => 10,
                'total_page' => 3
            ),
            'guest_data' => $guests,
        );


        return response()
            ->json($result);
    }

    public function get($id) {
        /*--- Get config ---*/
        $file_cd = $config = config('constants.FILE_CD');
        $file_cd_avatar = $file_cd['GUEST_AVATAR'];
        $file_cd_attach = $file_cd['GUEST_ATTACH'];

        $config = config('constants.UPLOAD_FILE');
        $upload_path = $config['PATH'];
        $env = $config['ENV'];
        /*--- Get config - END ---*/

        $guest_data = Guest::get_for_detail($id);

        if ($id != 0 && empty($guest_data)) {
            return response()->error('Not found', 404);
        }

        $genders = array(
            0   =>  '--- All ---',
            1   =>  'Male',
            2   =>  'Female',
            3   =>  'Other',
        );

        $guest_data->gender = isset($guest_data->sex) ? $genders[$guest_data->sex] : $genders[0];
        if (!empty($guest_data)) {
            /*Get avatar*/
            $avatar = FileMst::get_file($file_cd_avatar, $guest_data->guest_id);

            if (!empty($avatar)) {
                $guest_data->avatar = $upload_path.$env.$config['ROOT_GUEST_AVATAR'].$avatar->file_name;
            }

            /*Get attach*/
            $attach = FileMst::get_file($file_cd_attach, $guest_data->guest_id);

            if (!empty($attach)) {
                $guest_data->attach = $upload_path.$env.$config['ROOT_GUEST_ATTACH'].$attach->file_name;
            }
        }

        return response()
            ->json($guest_data);

    }

    public function form($id) {

        /*--- Get config ---*/
        $file_cd = $config = config('constants.FILE_CD');
        $file_cd_avatar = $file_cd['GUEST_AVATAR'];
        $file_cd_attach = $file_cd['GUEST_ATTACH'];

        $config = config('constants.UPLOAD_FILE');
        $upload_path = $config['PATH'];
        $env = $config['ENV'];
        /*--- Get config - END ---*/

        $guest_info = Guest::get_for_form($id);

        if ($id != 0 && empty($guest_info)) {
            return response()->error('Not found', 404);
        }

        if (!empty($guest_info)) {
            /*Get avatar*/
            $avatar = FileMst::get_file($file_cd_avatar, $guest_info->guest_id);

            if (!empty($avatar)) {
                $guest_info->avatar_img = $upload_path.$env.$config['ROOT_GUEST_AVATAR'].$avatar->file_name;
            }

            /*Get attach*/
            $attach = FileMst::get_file($file_cd_attach, $guest_info->guest_id);

            if (!empty($attach)) {
                $guest_info->attach_img = $upload_path.$env.$config['ROOT_GUEST_ATTACH'].$attach->file_name;
            }
        }

        $rooms = Room::select_for_search();

        $result = array(
            'guest' => $guest_info,
            'rooms' => $rooms,
        );

        return response()
            ->json($result);

    }


    public function save() {

        /*--- Get config ---*/
        $file_cd = $config = config('constants.FILE_CD');
        $file_cd_avatar = $file_cd['GUEST_AVATAR'];
        $file_cd_attach = $file_cd['GUEST_ATTACH'];

        $config = config('constants.UPLOAD_FILE');
        /*--- Get config - END ---*/

        $guest_id = isset($_POST['guest_id']) ? $_POST['guest_id'] : 0;

        $data = array(
            'room_id' => isset($_POST['room_id']) ? $_POST['room_id'] : 0,
            'first_name' => isset($_POST['first_name']) ? $_POST['first_name'] : '',
            'last_name' => isset($_POST['last_name']) ? $_POST['last_name'] : '',
            'phone' => isset($_POST['phone']) ? $_POST['phone'] : '',
            'sex' => isset($_POST['sex']) ? $_POST['sex'] : 0,
            'address' => isset($_POST['address']) ? $_POST['address'] : '',
            'birthday' => isset($_POST['birthday']) ? $_POST['birthday'] : '',
            'identity_card_no' => isset($_POST['identity_card_no']) ? $_POST['identity_card_no'] : '',
            'job_name' => isset($_POST['job_name']) ? $_POST['job_name'] : '',
            'residential_reg_flg' => isset($_POST['residential_reg_flg']) ? $_POST['residential_reg_flg'] : 0,
            'mod_func_id' => 'guest_save',
            'mod_ts' => date('Y-m-d H:i:s'),
            'mod_user_id' => 1,
            'version_no' => isset($_POST['version_no']) ? $_POST['version_no'] : 1,
        );

        $result = 0;
        if ($guest_id === 0) {
            //TODO: Insert

            $data['cre_func_id'] = 'guest_save';
            $data['cre_ts'] = date('Y-m-d H:i:s');
            $data['cre_user_id'] = 1;

            $result = $guest_id = Guest::insert_guest($data);
        } else {
            //TODO: Update

            $version_no = $data['version_no'];
            $data['version_no'] = $version_no + 1;

            $result = Guest::update_guest($guest_id, $version_no, $data);
        }


        /*--- Upload file ---*/
        if (isset($_FILES['avatar']['error']) && $_FILES['avatar']['error'] == 0) {
            //Exist avatar file...
            $ext = explode('.', $_FILES['avatar']['name']);
            $name = $_FILES['avatar']['name'];
            $file = $_FILES['avatar']['tmp_name'];
            $path = CommonModel::upload_file($name, $file, $config['ROOT_GUEST_AVATAR']);
            if ($path) {
                //Upload successful
                $avatar_data = FileMst::get_file($file_cd_avatar, $guest_id);
                if (empty($avatar_data)) {
                    //TODO: Insert
                    $avatar_data = array(
                        'account_id' => 1,
                        'file_name' => $path,
                        'file_cd' => $file_cd_avatar,
                        'relation_id' => $guest_id,
                        'cre_func_id' => 'guest_save',
                        'cre_ts' => date('Y-m-d H:i:s'),
                        'cre_user_id' => 1,
                        'mod_func_id' => 'guest_save',
                        'mod_ts' => date('Y-m-d H:i:s'),
                        'mod_user_id' => 1,
                        'version_no' => 1,
                    );

                    FileMst::insert_file($avatar_data);
                } else {
                    //TODO: Update
                    $version_no = $avatar_data->version_no;

                    $data_update = array(
                        'account_id' => 1,
                        'file_name' => $path,
                        'mod_func_id' => 'guest_save',
                        'mod_ts' => date('Y-m-d H:i:s'),
                        'mod_user_id' => 1,
                        'version_no' => $version_no + 1,
                    );

                    FileMst::update_file($avatar_data->file_id, $version_no, $data_update);
                }
            }
        }

        if (isset($_FILES['attach']['error']) && $_FILES['attach']['error'] == 0) {
            //Exist avatar file...
            $ext = explode('.', $_FILES['attach']['name']);
            $name = $_FILES['attach']['name'];
            $file = $_FILES['attach']['tmp_name'];
            $path = CommonModel::upload_file($name, $file, $config['ROOT_GUEST_ATTACH']);
            if ($path) {
                //Upload successful
                $attach_data = FileMst::get_file($file_cd_attach, $guest_id);

                if (empty($attach_data)) {
                    //TODO: Insert
                    $attach_data = array(
                        'account_id' => 1,
                        'file_name' => $path,
                        'file_cd' => $file_cd_attach,
                        'relation_id' => $guest_id,
                        'cre_func_id' => 'guest_save',
                        'cre_ts' => date('Y-m-d H:i:s'),
                        'cre_user_id' => 1,
                        'mod_func_id' => 'guest_save',
                        'mod_ts' => date('Y-m-d H:i:s'),
                        'mod_user_id' => 1,
                        'version_no' => 1,
                    );

                    FileMst::insert_file($attach_data);
                } else {
                    //TODO: Update
                    $version_no = $attach_data->version_no;

                    $data_update = array(
                        'account_id' => 1,
                        'file_name' => $path,
                        'mod_func_id' => 'guest_save',
                        'mod_ts' => date('Y-m-d H:i:s'),
                        'mod_user_id' => 1,
                        'version_no' => $version_no + 1,
                    );

                    FileMst::update_file($attach_data->file_id, $version_no, $data_update);
                }
            }
        }

        /*--- Upload file - END ---*/

        return response($result);

    }

    public function delete(Request $request) {

        $this->validate($request, [
            'id'   => 'required',
            'version_no'   => 'required',
        ]);

        $resut = Guest::delete_guest($request->id, $request->version_no);

        return response($resut);

    }
}