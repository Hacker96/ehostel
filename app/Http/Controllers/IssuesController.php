<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\GuestIssues;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class IssuesController extends Controller
{
    //

    public function get($id = null){
    	if($id == null){
    		return DB::table('guest_issues')
    		->join('guest', 'guest_issues.guest_id', '=', 'guest.guest_id')
    		->join('room', 'guest.room_id', '=', 'room.room_id')
    		->select('guest_issues.issues_id', 'guest_issues.issue_date', 'guest_issues.content', 'guest.first_name', 'guest.last_name', 'room.room_name')
    		->get();
    	}
		else
			return DB::table('guest_issues')
            ->join('guest', 'guest_issues.guest_id', '=', 'guest.guest_id')
            ->join('room', 'guest.room_id', '=', 'room.room_id')
            ->select('guest_issues.issues_id', 'room.room_id', 'guest_issues.issue_date', 'guest_issues.content', 'guest.guest_id', 'guest.first_name', 'guest.last_name', 'room.room_name')
            ->where('issues_id', '=', $id)
            ->get();
    	
    }
    public function store(Request $request) {
        $guest_issues = new GuestIssues;

        $guest_issues->guest_id = $request->input('guest_id');
        $guest_issues->issue_date = date("Y-m-d");
        $guest_issues->content = $request->input('content');
        $guest_issues->cre_func_id = '0';
        $guest_issues->cre_user_id = 1;
        $guest_issues->mod_func_id = '0';
        $guest_issues->mod_user_id = 1;

        $guest_issues->save();
    }
    public function update(Request $request, $id) {

        GuestIssues::where('issues_id', $id)->update([
            'guest_id'  => $request->input('guest_id'),
            'content'   => $request->input('content')
        ]);
    }
}
