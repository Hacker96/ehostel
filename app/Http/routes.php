<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'AngularController@serveApp');

    Route::get('/unsupported-browser', 'AngularController@unsupported');

    Route::get('/admin', 'AdminController@index');

});

$api->group([], function ($api) {
    $api->post('posts', 'PostsController@create');
});

//public API routes
$api->group(['middleware' => ['api']], function ($api) {

    // Authentication Routes...
//    $api->post('auth/login', 'Auth\AuthController@login');
    $api->post('auth/login', 'Auth\AuthController@admin_login');
    $api->post('auth/register', 'Auth\AuthController@register');

    // Password Reset Routes...
    $api->post('auth/password/email', 'Auth\PasswordResetController@sendResetLinkEmail');
    $api->get('auth/password/verify', 'Auth\PasswordResetController@verify');
    $api->post('auth/password/reset', 'Auth\PasswordResetController@reset');

});


//protected API routes with JWT (must be logged in)
$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
//    $api->get('/api_required_autheticated', 'RoomController@collect');

    /*Define API here...*/
    //TODO...

    /* User*/
    //Change password
    $api->post('auth/change-password', 'Auth\AuthController@change_password');

    /* User - END*/

    /* Room */
    //Get list room
    $api->get('room/collect', 'RoomController@collect');
//    $api->get('room/get', 'RoomController@get');
    $api->get('room/get/{id}', 'RoomController@get');
    /* Room - END */


    Route::get('api/issues/get/{id?}', 'IssuesController@get');
    Route::post('api/issues/store/{id?}', 'IssuesController@store');
    Route::post('api/issues/update/{id?}', 'IssuesController@update');
    Route::get('api/zoom/get/{id?}', 'RoomController@getlist');
    Route::get('api/guest/get/room/{id?}', 'GuestController@getlist');
    /* Guest */
    $api->get('guest/guest-collect', 'GuestController@collect');
    $api->get('guest/get/{id}', 'GuestController@get');
    $api->get('guest/form/{id}', 'GuestController@form');
    $api->post('guest/save', 'GuestController@save');
    $api->post('guest/delete', 'GuestController@delete');
    /* Guest - END */

    /* Revenue */
    $api->get('revenue/revenue-collect', 'FinanceRevenueController@collect');
    $api->get('revenue/get/{id}', 'FinanceRevenueController@get');
    $api->get('revenue/form/{id}', 'FinanceRevenueController@form');
    $api->post('revenue/save', 'FinanceRevenueController@save');
    $api->post('revenue/delete', 'FinanceRevenueController@delete');
    /* Revenue - END */

    /*Define API here... - END*/
});


//Route::auth();
//Route::get('/home', 'HomeController@index');
