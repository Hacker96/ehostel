<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FuncConf extends Model
{
    //
    protected $fillable = ['func_conf_key', 'account_id', 'user_id', 'row_no', 'idx_arr', 'context', 'cre_func_id', 'cre_ts', 'cre_user_id', 'mod_func_id', 'mod_ts', 'mod_user_id', 'version_no', 'del_flg'];
}