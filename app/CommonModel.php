<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class CommonModel extends Model
{
    //

    public static function upload_file($name, $file, $root = '') {
        /*Config*/
        $config = config('constants.UPLOAD_FILE');

        $upload_path = $config['PATH'];
        $env = $config['ENV'];
        /*Config - END*/

        $rs2 = '/'.date("YmdHis").'-'.$name;

        $paths = $upload_path.$env.$root;
        $paths .= $rs2;
        // upload the file to the path specified

        $upload = move_uploaded_file($file, $paths);

        // check the upload status
        if (!$upload) {
            return FALSE;
        } else {
            return $rs2;
        }

        return FALSE;
    }
}
