<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FileMst extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'file_mst';
    //Define primary key
    protected $primaryKey = 'file_id';


    public static function get_file($file_cd, $relation_id) {
        $result = DB::table('file_mst')
            ->where([
                ['file_cd', '=', $file_cd],
                ['relation_id', '=', $relation_id],
                ['del_flg', '=', 0],
            ])
            ->first();

        return $result;
    }


    public static function insert_file($data) {
        return DB::table('file_mst')->insert($data);
    }


    public static function update_file($id, $version_no, $data) {
        return DB::table('file_mst')
            ->where([
                ['file_id', '=', $id],
                ['version_no', '=', $version_no]
            ])
            ->update($data);
    }
}
