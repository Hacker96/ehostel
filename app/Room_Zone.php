<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room_Zone extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
//    protected $table = 'room';

    //Define primary key
    protected $primaryKey = 'zone_id';

    //incrementing
    protected $incrementing = TRUE;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    protected $timestamps = FALSE;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The connection name for the model.
     *
     * @var string
     */
//    protected $connection = 'connection-name';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id', 'zone_name', 'address',
        'cre_func_id', 'cre_ts', 'cre_user_id', 'mod_func_id', 'mod_ts', 'mod_user_id',
        'version_no', 'del_flg'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
//    protected $dates = ['del_flg'];
}
