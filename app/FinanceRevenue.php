<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FinanceRevenue extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'revenue';
    //Define primary key
    protected $primaryKey = 'revenue_id';

    public static function select_revenue_search($year,$month,$room,$offset,$limit){

//        $results = DB::select('select * from users where id = :id', ['id' => 1]);

        $sql = 'select * from revenue where del_flg = :flag ';
        $condition = [];
        $condition['flag'] = 0;


        if ($year != 0 && $month != 0) {
            $sql .= " AND revenue_year_month =:revenue_date ";
            $condition['revenue_date'] = $year.$month;
        }elseif($year != 0){
            $sql .= " AND revenue_year_month Like :revenue_date ";
            $condition['revenue_date'] = "%".$year."%";
        }elseif($month != 0){
            $sql .= " AND revenue_year_month Like :revenue_date ";
            $condition['revenue_date'] = "%".date("Y").$month."%";
        }

        if ($room != 0) {
            $sql .= " AND room_id = :room_id ";
            $condition['room_id'] = $room;
        }
        //LIMIT 10 OFFSET 15
        $condition['limit'] = $limit;
        $condition['offset'] = $offset;
        //
        $sql .= " order by revenue_year_month desc ";
        $sql .= " limit :limit ";
        $sql .= " offset :offset ";


        //echo $offset;
        //echo $limit;
        //echo $sql;


        return DB::select($sql, $condition);
    }
    public function get($id) {
        //
        $result =  FinanceRevenue::where('revenue_id', $id)
            ->where('del_flg', 0)->first();

        return response()
            ->json($result);
    }
    public static function get_revenue_by_room($room_id) {
        return DB::table('revenue')
            ->join('revenue', function ($join) {
                $join->on('revenue.room_id', '=', 'room.room_id')->where('room.del_flg', '=', '0');
            })
            ->where([
                ['revenue.room_id', '=', $room_id],
                ['revenue.del_flg', '=', 0],
            ])
            ->select('revenue.*')
            ->orderBy('revenue.revenue_year_month', 'desc')
            ->get();
    }

}
