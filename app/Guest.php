<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Guest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'guest';
    //Define primary key
    protected $primaryKey = 'guest_id';

    public static function select_guest_search($name,$room,$gender, $offset, $limit){

//        $results = DB::select('select * from users where id = :id', ['id' => 1]);

        $sql = 'select * from guest where del_flg = :flag ';
        $condition = [];
        $condition['flag'] = 0;


        if (!empty($name)) {
            $sql .= " AND (first_name Like :name Or last_name Like :name1 ) ";
            $condition['name'] = "%".$name."%";
            $condition['name1'] = "%".$name."%";
        }

        if ($room != 0) {
            $sql .= " AND room_id = :room_id ";
            $condition['room_id'] = $room;
        }

        if ($gender != 0) {
            $sql .= " AND sex = :sex ";
            $condition['sex'] = $gender;
        }

        return DB::select($sql, $condition);
    }

    public static function get_for_detail($id) {
        $result = DB::table('guest')
            ->leftJoin('room', function ($join) {
                $join->on('guest.room_id', '=', 'room.room_id')->where('room.del_flg', '=', '0');
            })
            ->where([
                ['guest.guest_id', '=', $id],
                ['guest.del_flg', '=', 0],
            ])
            ->select('guest.*', 'room.room_name')
            ->first();

        return $result;
    }

    public static function get_for_form($id) {
        $result = Guest::where('guest_id', $id)
            ->where('del_flg', 0)->first();

        return $result;
    }

    public static function get_guest_by_room($room_id) {
        return DB::table('guest')
            ->join('room', function ($join) {
                $join->on('guest.room_id', '=', 'room.room_id')->where('room.del_flg', '=', '0');
            })
            ->leftJoin('file_mst', function ($join) {
                $join->on('guest.guest_id', '=', 'file_mst.relation_id')->where('file_mst.file_cd', '=', '01');
            })
            ->where([
                ['guest.room_id', '=', $room_id],
                ['guest.del_flg', '=', 0],
            ])
            ->select('guest.*', 'file_mst.file_name as avatar')
            ->orderBy('guest.last_name', 'asc')
            ->get();
    }


    public static function insert_guest($data) {
        return DB::table('guest')->insertGetId($data);
    }

    public static function update_guest($id, $version_no, $data) {
        return DB::table('guest')
            ->where([
                ['guest_id', '=', $id],
                ['version_no', '=', $version_no]
            ])
            ->update($data);
    }

    public static function delete_guest($id, $version_no) {
        return DB::table('guest')
            ->where([
                ['guest.guest_id', '=', $id],
                ['guest.version_no', '=', $version_no]
            ])
            ->update(['del_flg' => 1]);
    }
}
