<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Room extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'room';

    //Define primary key
    protected $primaryKey = 'room_id';

    //incrementing
//    protected $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
//    protected $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
//    protected $dateFormat = 'U';

    /**
     * The connection name for the model.
     *
     * @var string
     */
//    protected $connection = 'connection-name';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id', 'room_name', 'effective_date', 'price', 'zone_id',
        'cre_func_id', 'cre_ts', 'cre_user_id', 'mod_func_id', 'mod_ts', 'mod_user_id',
        'version_no', 'del_flg'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['del_flg'];

    public static function get($zone_id = 1){
        return DB::table('room')->where('zone_id', '=', $zone_id)->get();
    }

    public static function collect() {
        $result = DB::table('room')
            ->leftJoin('room_zone', function ($join) {
                $join->on('room.zone_id', '=', 'room_zone.zone_id')->where('room_zone.del_flg', '=', '0');
            })
            ->leftJoin('account_contract', function($join) {
                $join->on('room.room_id', '=', 'account_contract.room_id')->where('account_contract.del_flg', '=', '0');
            })
            ->select('room.room_id', 'room.room_name', 'room_zone.zone_id', 'room_zone.zone_name', 'room_zone.address', 'account_contract.start_date', 'account_contract.end_date')
            ->get();

        return $result;
    }


    public static function select_for_search() {
        $result = DB::table('room')
            ->select('room_id','room_name')
            ->where('del_flg', '=', 0)
            ->get();

        return $result;
    }

}
