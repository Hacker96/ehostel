<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contract extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account_contract';

    //Define primary key
    protected $primaryKey = 'account_contract_id';

    public static function get_contract_for_room_detail($room_id){

        return DB::table('account_contract')
            ->join('guest', function ($join) {
                $join->on('account_contract.guest_id', '=', 'guest.guest_id')->where('guest.del_flg', '=', '0');
            })
            ->where([
                ['account_contract.room_id', '=', $room_id],
                ['account_contract.del_flg', '=', 0],
            ])
            ->select('account_contract.*', 'guest.first_name', 'guest.last_name')
            ->first();
    }
}
