<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CdMst extends Model
{
    protected $fillable = ['cd_mst','cd_key','cd','disp_order','ext_val','cre_func_id','cre_ts','cre_user_id','mod_func_id','mod_ts','mod_user_id','version_no','del_flg','0'];
}