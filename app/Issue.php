<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestIssues extends Model
{
    //
    protected $fillable = ['issues_id', 'guest_id', 'issue_date', 'content', 'cre_func_id', 'cre_ts', 'mod_ts', 'cre_user_id', 'mod_func_id', 'mod_user_id', 'version_no', 'del_flg' ];
}
