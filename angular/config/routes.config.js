export function RoutesConfig($stateProvider, $urlRouterProvider) {
	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}/${viewName}.page.html`;
	};

    let getLayout = (layout) => {
        return `./views/app/components/${layout}/${layout}.component.html`;
    }

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('app', {
			abstract: true,
            data: {
                bodyClass: 'fixed skin-blue sidebar-mini ehostel'
                //{auth: true} // would require JWT auth
            },
			views: {
                layout: {
                    templateUrl: getLayout('app-view')
                },
                'header@app': {
					templateUrl: getView('header-admin')
                },
                'footer@app': {
					templateUrl: getView('footer-admin')
                },
                'sidebar@app': {
                    templateUrl: getView('sidebar-admin')
                },
				main: {}
			}
		})
        .state('app.home', {
            url: '/',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('room-main')
                }
            }
        })
        //.state('app.login', {
		//	url: '/login',
		//	views: {
		//		'main@app': {
		//			templateUrl: getView('login')
		//		}
		//	}
		//})
        .state('app.login', {
            url: '/login',
            data: {
                bodyClass: 'hold-transition login-page'
            },
            views: {
                'layout@': {
                    templateUrl: getView('admin-login')
                }
            }
        })
        .state('app.logout', {
            url: '/logout',
            views: {
                'main@app': {
                    template: '<admin-logout></admin-logout>'
                }
            }
        })
        .state('app.register', {
            url: '/register',
            views: {
                'main@app': {
                    templateUrl: getView('register')
                }
            }
        })
        .state('app.forgot_password', {
            url: '/forgot-password',
            views: {
                'main@app': {
                    templateUrl: getView('forgot-password')
                }
            }
        })
        .state('app.reset_password', {
            url: '/reset-password/:email/:token',
            views: {
                'main@app': {
                    templateUrl: getView('reset-password')
                }
            }
        })
        .state('app.create_post', {
            url: '/create-post',
            views: {
                'main@app': {
                    templateUrl: getView('create_post')
                }
            }
        })
        .state('app.room-main', {
            url: '/room-main',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('room-main')
                }
            }
        })
        .state('app.room-crud', {
            url: '/room-crud',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('room-crud')
                }
            }
        })
        .state('app.room-detail', {
            url: '/room-detail/:id',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('room-detail')
                }
            }
        })
        .state('app.contract-main', {
            url: '/contract-main',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('contract-main')
                }
            }
        })
        .state('app.contract-detail', {
            url: '/contract-detail',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('contract-detail')
                }
            }
        })
        .state('app.contract-form', {
            url: '/contract-form',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('contract-form')
                }
            }
        })
        .state('app.contract-liquidate', {
            url: '/contract-liquidate',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('contract-liquidate')
                }
            }
        })
        .state('app.finance-revenue', {
            url: '/finance-revenue',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('finance-revenue')
                }
            }
        })
        .state('app.finance-revenue-form', {
            url: '/finance-revenue-form',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('finance-revenue-form')
                }
            }
        })
        .state('app.finance-revenue-detail', {
            url: '/finance-revenue-detail/:id',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('finance-revenue-detail')
                }
            }
        })
        .state('app.finance-revenue-payment', {
            url: '/finance-revenue-payment',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('finance-revenue-payment')
                }
            }
        })
        .state('app.finance-cost', {
            url: '/finance-cost',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('finance-cost')
                }
            }
        })
        .state('app.finance-cost-form', {
            url: '/finance-cost-form/:id',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('finance-cost-form')
                }
            }
        })
        .state('app.guest-main', {
            url: '/guest-main',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('guest-main')
                }
            }
        })
        .state('app.guest-detail', {
            url: '/guest-detail/:id',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('guest-detail')
                }
            }
        })
        .state('app.guest-form', {
            url: '/guest-form/:id',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('guest-form')
                }
            }
        })
        .state('app.issue-main', {
            url: '/issue-main',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('issue-main')
                }
            }
        })
        .state('app.issue-edit', {
            url: '/issue-edit/:id',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('issue-edit')
                }
            }
        })
        .state('app.issue-detail', {
            url: '/issue-detail/:id',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('issue-detail')
                }
            }
        })
        .state('app.dashboard', {
            url: '/dashboard',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('dashboard')
                }
            }
        })
        .state('app.employee', {
            url: '/employee',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('employee')
                }
            }
        })
        .state('app.employee-edit', {
            url: '/employee-edit',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('employee-edit')
                }
            }
        })
        .state('app.add-employee', {
            url: '/add-employee',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('add-employee')
                }
            }
        })
        .state('app.employee-role', {
            url: '/employee-role',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('employee-role')
                }
            }
        })
        .state('app.add-issue', {
            url: '/add-issue',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('add-issue')
                }
            }
        })
        .state('app.config-form', {
            url: '/config-form',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('config-form')
                }
            }
        })
        .state('app.ehostel-main', {
            url: '/ehostel-main',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('ehostel-main')
                }
            }
        })
        .state('app.ehostel-contract-add', {
            url: '/ehostel-contract-add',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('ehostel-contract-add')
                }
            }
        })
        .state('app.ehostel-contract-detail', {
            url: '/ehostel-contract-detail',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('ehostel-contract-detail')
                }
            }
        })
        .state('app.ehostel-contract-edit', {
            url: '/ehostel-contract-edit',
            data: {auth: true},
            views: {
                'main@app': {
                    templateUrl: getView('ehostel-contract-edit')
                }
            }
        })
    ;
}
