class
GuestMainController
{
    constructor(API, ToastService, $state)
    {
        'ngInject';

        //
        this.API = API;
        this.ToastService = ToastService;
        this.$state = $state;

        /* Sidebar */
        angular.element('.treeview').removeClass('active');
        angular.element('.treeview-menu').removeClass('menu-open');
        angular.element('.treeview-menu').hide();
        /* Sidebar - END*/

        angular.element(".select2").select2();
    }

    $onInit()
    {
        this.search_name = '';
        this.search_gender = '0';
        this.search_room = 0;
        this.search_page_size = '10';
        this.search_page_index = 0;
        this.data = [];

        this.searchData(0);
    }

    searchData(is_search)
    {
        let search_name = this.search_name;
        let search_gender = this.search_gender;
        let search_room = this.search_room;
        let search_page_size = this.search_page_size;
        let search_page_index = this.search_page_index;

        this.API.all('guest').get('guest-collect', {is_search, search_name, search_gender, search_room, search_page_size, search_page_index}).then((response) => {
            if (is_search == 0)
            {
                this.data.rooms = response.rooms;
                this.data.genders = response.genders;
            }

            this.data.paging_info = response.paging_info;
            this.data.guest_data = response.guest_data;
        });
    }

    deleteGuest(id, version_no)
{
        if (confirm('Do you want to delete this items?')) {
            let data = {
                id: id,
                version_no: version_no
            };

            this.API.all('guest/delete').post(data).then(() => {
                this.$state.reload();
                this.ToastService.show('Delete successful.');
            },() =>
            {
                this.ToastService.error('Delete failed!');
            });
        }
    }
}

export const GuestMainComponent = {
    templateUrl: './views/app/components/guest-main/guest-main.component.html',
    controller: GuestMainController,
    controllerAs: 'vm',
    bindings: {}
}
