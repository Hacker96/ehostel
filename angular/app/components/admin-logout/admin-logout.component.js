class AdminLogoutController{
    constructor($rootScope,$auth,$state){
        'ngInject';

        //

        $auth.logout().then(function () {
            delete $rootScope.me;
            //AclService.flushRoles()
            //AclService.setAbilities({})
            $state.go('app.login');
        });
    }

    $onInit(){
    }
}

export const AdminLogoutComponent = {
    templateUrl: './views/app/components/admin-logout/admin-logout.component.html',
    controller: AdminLogoutController,
    controllerAs: 'vm',
    bindings: {}
}
