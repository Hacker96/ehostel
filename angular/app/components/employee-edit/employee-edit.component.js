class EmployeeEditController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const EmployeeEditComponent = {
    templateUrl: './views/app/components/employee-edit/employee-edit.component.html',
    controller: EmployeeEditController,
    controllerAs: 'vm',
    bindings: {}
}
