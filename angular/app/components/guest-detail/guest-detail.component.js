class GuestDetailController{
    constructor($state, ToastService, API){
        'ngInject';

        //
        this.API = API;
        this.$state = $state;
        this.ToastService = ToastService;

        // id
        let id = this.$state.params.id;
        this.getDataGuest(id);
    }

    $onInit(){

    }

    getDataGuest(id){
        let data = this.API.service('get', this.API.all('guest'));

        data.one(id).get().then((response) => {
            this.guest = response;
        }, () => {
            this.$state.go('app.guest-main');
        });
    }
}

export const GuestDetailComponent = {
    templateUrl: './views/app/components/guest-detail/guest-detail.component.html',
    controller: GuestDetailController,
    controllerAs: 'vm',
    bindings: {}
}
