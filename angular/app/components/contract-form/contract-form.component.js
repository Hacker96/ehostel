class ContractFormController{
    constructor($state){
        'ngInject';

        this.$state = $state;

        //
        //Date range picker
        //angular.element('.reservation').daterangepicker();

        //Date picker
        angular.element('.datepicker').datepicker({
            autoclose: true
        });
    }

    $onInit(){
    }

    save_contract(){
        this.$state.go('app.contract-main');
    }
}

export const ContractFormComponent = {
    templateUrl: './views/app/components/contract-form/contract-form.component.html',
    controller: ContractFormController,
    controllerAs: 'vm',
    bindings: {}
}
