class HeaderAdminController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const HeaderAdminComponent = {
    templateUrl: './views/app/components/header-admin/header-admin.component.html',
    controller: HeaderAdminController,
    controllerAs: 'vm',
    bindings: {}
}
