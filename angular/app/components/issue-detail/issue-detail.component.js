class IssueDetailController{
    constructor(API, ToastService, $state, $scope, $window, $http, $q){
        'ngInject';

        //
                
        angular.element(function () {
            angular.element('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });

        this.API = API;
        this.$state = $state;
        this.ToastService = ToastService;
        // id
        let id = this.$state.params.id;
        this.getDataIssue(id);
    }
    getDataIssue(id){
        let data = this.API.service('get', this.API.all('issues'));

        data.one(id).get().then((response) => {
            this.issues = response;
            console.log(response);
        }, () => {
            this.$state.go('app.issue-main');
        });
    }
    $onInit(){
    }
}

export const IssueDetailComponent = {
    templateUrl: './views/app/components/issue-detail/issue-detail.component.html',
    controller: IssueDetailController,
    controllerAs: 'vm',
    bindings: {}
}
