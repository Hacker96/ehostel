class ContractMainController{
    constructor(){
        'ngInject';

        //
        /* Sidebar */
        angular.element('.treeview').removeClass('active');
        angular.element('.treeview-menu').removeClass('menu-open');
        angular.element('.treeview-menu').hide();
        /* Sidebar - END*/

        angular.element(".contract-delete").click(function(){
            if (confirm("Are you sure delete this item?")) {
                alert('Deleting...');
            }
        });

        //Date range picker
        angular.element('.reservation').daterangepicker();

        //Check box
        angular.element('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    }

    $onInit(){
    }
}

export const ContractMainComponent = {
    templateUrl: './views/app/components/contract-main/contract-main.component.html',
    controller: ContractMainController,
    controllerAs: 'vm',
    bindings: {}
}
