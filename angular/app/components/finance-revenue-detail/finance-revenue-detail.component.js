class FinanceRevenueDetailController{
    constructor($state, ToastService, API){
        'ngInject';

        //
        this.API = API;
        this.$state = $state;

        this.ToastService = ToastService;

        this.year_month = '';


        this.status = 0;
    }

    $onInit(){
        // id
        let id = this.$state.params.id;
        this.getRevenueById(id);

    }

    getRevenueById(id){
        let data = this.API.service('get', this.API.all('revenue'));

        data.one(id).get().then((response) => {
            this.revenue = response.revenue;
            this.year_month = this.getDateFormat(response.revenue.revenue_year_month);
            this.room = response.room;
        }, () => {
            this.$state.go('app.finance-revenue');
        });

    }

    getDateFormat(param){
        if(param.length == 6){
            return param.substr(0,4) + '/' + param.substr(4,2);
        }else{
            return '';
        }
    }
}

export const FinanceRevenueDetailComponent = {
    templateUrl: './views/app/components/finance-revenue-detail/finance-revenue-detail.component.html',
    controller: FinanceRevenueDetailController,
    controllerAs: 'vm',
    bindings: {}
}
