class AddIssueController{
    constructor(API, ToastService, $state, $scope, $window, $http, $q){
        'ngInject';

        //
        this.API = API;
        this.$state = $state;
        this.ToastService = ToastService;
        this.$scope = $scope;
        this.$window = $window;
        this.$http = $http;
        this.$q = $q;


        angular.element(function () {
            angular.element('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
            angular.element(".select2").select2();
        });
        this.guest_issues = {};
        this.guestList = {};
        this.rooms = this.getRoom();
    }

    $onInit(){
    }
    getRoom(){
        return this.API.all('zoom/get/').getList().$object;
    }
    getGuest(id){
        return this.API.all('guest/get/room/' + id).getList().$object;
    }
    loadGuests(id){
        this.guestList = this.getGuest(id);
    }


    saveIssue(){
        var fd = new FormData();
        angular.forEach(this.guest_issues, function(value, key) {
            fd.append(key, value);
        });

        this.postIssue(fd);

        this.$state.go('app.issue-main');
        this.ToastService.show('Successful');
    }

    postIssue(fd) {
        var rs = this.$q.defer();
        this.$http.post('api/issues/store', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
            }).success(function(){
                rs.resolve(1);
            })
            .error(function(){
                rs.reject(0)
            });

        return rs.promise;
    }
}

export const AddIssueComponent = {
    templateUrl: './views/app/components/add-issue/add-issue.component.html',
    controller: AddIssueController,
    controllerAs: 'vm',
    bindings: {}
}
