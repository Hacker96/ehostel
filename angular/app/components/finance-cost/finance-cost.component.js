class FinanceCostController{
    constructor(){
        'ngInject';

        //

        angular.element(".cost-delete").click(function(){
            if (confirm("Are you sure delete this item?")) {
                alert('Deleting...');
            }
        });

        //Date range picker
        angular.element('.reservation').daterangepicker();
    }

    $onInit(){
    }
}

export const FinanceCostComponent = {
    templateUrl: './views/app/components/finance-cost/finance-cost.component.html',
    controller: FinanceCostController,
    controllerAs: 'vm',
    bindings: {}
}
