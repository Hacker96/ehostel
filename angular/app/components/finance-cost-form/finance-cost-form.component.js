class FinanceCostFormController{
    constructor($state){
        'ngInject';

        this.$state = $state;

        //

        angular.element('.datepicker').datepicker({
            autoclose: true
        });
    }

    $onInit(){
    }

    save_cost(){
        this.$state.go('app.finance-cost');
    }
}

export const FinanceCostFormComponent = {
    templateUrl: './views/app/components/finance-cost-form/finance-cost-form.component.html',
    controller: FinanceCostFormController,
    controllerAs: 'vm',
    bindings: {}
}
