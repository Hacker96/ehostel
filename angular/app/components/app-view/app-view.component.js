class AppViewController {
    constructor($rootScope, $state, API, $mdToast, ToastService, $window) {
        'ngInject';

        this.$rootScope = $rootScope;
        this.$state = $state;
        this.API = API;
        this.$mdToast = $mdToast;
        this.ToastService = ToastService;
        this.$window = $window;
    }

    $onInit() {

        this.password = '';
        this.new_password = '';
        this.confirm_password = '';

        this.registerServiceWorker();
        this.checkForNewerVersions();
    }

    registerServiceWorker() {
        if (!('serviceWorker' in navigator)) {
            return false;
        }
        navigator.serviceWorker.register('/service-worker.js')
            .then(this.handleRegistration.bind(this));
    }

    handleRegistration(registration) {
        registration.onupdatefound = () => {
            const installingWorker = registration.installing;
            installingWorker.onstatechange = () => {
                if (installingWorker.state === 'installed') {
                    if (!navigator.serviceWorker.controller) {
                        this.ToastService.show('App is ready for offline use.');
                    }
                }
            }
        }
    }

    checkForNewerVersions() {
        if (navigator.serviceWorker && navigator.serviceWorker.controller) {
            navigator.serviceWorker.controller.onstatechange = (e) => {

                if (e.target.state === 'redundant') {
                    let toast = this.$mdToast.simple()
                        .content('A newer version of this site is available.')
                        .position(this.ToastService.position)
                        .action('Refresh')
                        .hideDelay(this.ToastService.delay);

                    this.$mdToast.show(toast).then(() => {
                        this.$window.location.reload();
                    });
                }
            };
        }
    }

    change_password() {

        let data = {
            password: this.password,
            new_password: this.new_password,
            confirm_password: this.confirm_password
        };

        this.API.all('auth/change-password').post(data).then((response) => {
            if (response.data) {
                angular.element('#updatePasswordModal').modal('hide');
                //delete this.$rootScope.tableformate;
                //this.$state.go('app.login');
                this.ToastService.show('Change password successful.');
            } else {
                this.ToastService.error('Change password failed!');
            }
        })
        .catch(this.failed_change_password.bind(this));
    }

    failed_change_password(response) {
        if (response.status === 422) {
            for (let error in response.data.errors) {
                return this.ToastService.error(response.data.errors[error][0]);
            }
        }
        this.ToastService.error(response.statusText);
    }
}

export const AppViewComponent = {
    templateUrl: './views/app/components/app-view/app-view.component.html',
    controller: AppViewController,
    controllerAs: 'vm',
    bindings: {}
}
