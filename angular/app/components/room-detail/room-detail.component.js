class RoomDetailController{
    constructor($state, API){
        'ngInject';

        this.$state = $state;
        this.API = API;
        this.is_show_note = false;
        this.room_data = [];
        this.guest_data = null;
        this.guest_data_id = 0;

        /*Tab*/
        angular.element(".nav-tabs a").click(function(e){
            e.preventDefault();
            angular.element(this).tab('show');
        });
        /*Tab - END*/
    }

    $onInit(){
        let id = this.$state.params.id;

        //alert(id);
        //this.API.all('room').get('get', {
        //    id
        //}).then((response) => {
        //    console.log(response.id);
        //});

        let UserData = this.API.service('get', this.API.all('room'));
        UserData.one(id).get()
            .then((response) => {
                this.room_data = response;

                if (this.room_data.guests.length != 0) {
                    this.selectGuest(this.room_data.guests[0].guest_id);
                }
            }, () => {
                this.$state.go('app.room-main');
            });
    }

    save_note() {
        this.is_show_note = false;
    }

    show_note() {
        this.is_show_note = true;
    }

    selectGuest(guest_id) {
        this.guest_data_id = guest_id;
        angular.element(".widget-user-2").show();

        let data = this.API.service('get', this.API.all('guest'));
        data.one(guest_id).get()
            .then((response) => {
            this.guest_data = response;
        }, () => {
            this.$state.go('app.room-main');
        });
    }
}

export const RoomDetailComponent = {
    templateUrl: './views/app/components/room-detail/room-detail.component.html',
    controller: RoomDetailController,
    controllerAs: 'vm',
    bindings: {}
}
