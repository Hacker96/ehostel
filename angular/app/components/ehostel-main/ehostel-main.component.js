class EhostelMainController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const EhostelMainComponent = {
    templateUrl: './views/app/components/ehostel-main/ehostel-main.component.html',
    controller: EhostelMainController,
    controllerAs: 'vm',
    bindings: {}
}
