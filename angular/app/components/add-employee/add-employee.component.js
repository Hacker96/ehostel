class AddEmployeeController{
    constructor(){
        'ngInject';

        //
        angular.element('.datepicker').datepicker();
    }

    $onInit(){
    }
}

export const AddEmployeeComponent = {
    templateUrl: './views/app/components/add-employee/add-employee.component.html',
    controller: AddEmployeeController,
    controllerAs: 'vm',
    bindings: {}
}
