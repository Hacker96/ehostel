class FinanceRevenueFormController{
    constructor($state){
        'ngInject';


        this.$state = $state;
        //

        angular.element('.datepicker').datepicker({
            autoclose: true
        });

        angular.element('.add-history').click(function(){
            angular.element('#add-history').before(
                '<div class="form-group">' +
                '<div class="col-sm-offset-2 col-sm-5">' +
                '<input type="number" class="form-control pull-right" id="received">' +
                '</div>' +
                '<div class="col-sm-5">' +
                '<input type="text" class="form-control pull-right datepicker" id="datepicker_add">' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<div class="col-sm-offset-2 col-sm-10">' +
                '<textarea class="form-control pull-right" id="note" placeholder="note..."></textarea>' +
                '</div>' +
                '</div>'
            );

            angular.element('.datepicker').datepicker({
                autoclose: true
            });
        });

        //finance
        this.finance = [];
    }

    $onInit(){
        this.finance = [];
        //this.finance.room_id =
    }

    save_revenue(){
        this.$state.go('app.finance-revenue');
    }
}

export const FinanceRevenueFormComponent = {
    templateUrl: './views/app/components/finance-revenue-form/finance-revenue-form.component.html',
    controller: FinanceRevenueFormController,
    controllerAs: 'vm',
    bindings: {}
}
