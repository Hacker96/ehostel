class EhostelContractEditController{
    constructor(){
        'ngInject';

        //
        angular.element('.datepicker').datepicker({
            autoclose: true
        });
    }

    $onInit(){
    }
}

export const EhostelContractEditComponent = {
    templateUrl: './views/app/components/ehostel-contract-edit/ehostel-contract-edit.component.html',
    controller: EhostelContractEditController,
    controllerAs: 'vm',
    bindings: {}
}
