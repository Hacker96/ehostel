class FooterAdminController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const FooterAdminComponent = {
    templateUrl: './views/app/components/footer-admin/footer-admin.component.html',
    controller: FooterAdminController,
    controllerAs: 'vm',
    bindings: {}
}
