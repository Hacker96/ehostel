class CreatePostFormController{
    constructor(API, ToastService){
        'ngInject';

        //
        this.API = API;

        this.ToastService = ToastService;

        this.name = '';
        this.topic = '';
    }

    submit(){
        if ( !this.name || !this.topic) {
            return false;
        }

        this.API.all('posts').post({name: this.name, topic:this.topic}).then(() => {
            this.ToastService.show('Post added successfully');
            this.name = '';
            this.name = '';
        });
    }

    $onInit(){
    }
}

export const CreatePostFormComponent = {
    templateUrl: './views/app/components/create_post_form/create_post_form.component.html',
    controller: CreatePostFormController,
    controllerAs: 'vm',
    bindings: {}
}
