class FinanceRevenueController{
    constructor(API, ToastService){
        'ngInject';

        //
        this.API = API;
        this.ToastService = ToastService;

        //
        angular.element(".select2").select2();
    }

    $onInit(){
        this.search_year = '0';
        this.search_month = '0';
        this.search_room = '0';
        this.search_page_size = '10';
        this.search_page_index = 0;
        this.data = [];
        this.room_maps = [];

        this.searchData(0);
    }

    searchData(is_search)
    {
        let search_year = this.search_year;
        let search_month = this.search_month;
        let search_room = this.search_room;
        let search_page_size = this.search_page_size;
        let search_page_index = this.search_page_index;

        this.API.all('revenue').get('revenue-collect', {is_search, search_year, search_month, search_room, search_page_size, search_page_index}).then((response) => {
            if (is_search == 0)
            {
                this.data.rooms = response.rooms;
                this.data.years = response.years;

                for(var i = 0; i < this.data.rooms.length; i++){
                    this.room_maps[this.data.rooms[i].room_id] = this.data.rooms[i].room_name;
                }
            }

            this.data.paging_info = response.paging_info;
            this.data.revenue_data = response.revenue_data;
        });

    }

}

export const FinanceRevenueComponent = {
    templateUrl: './views/app/components/finance-revenue/finance-revenue.component.html',
    controller: FinanceRevenueController,
    controllerAs: 'vm',
    bindings: {}
}
