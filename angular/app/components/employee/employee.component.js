class EmployeeController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const EmployeeComponent = {
    templateUrl: './views/app/components/employee/employee.component.html',
    controller: EmployeeController,
    controllerAs: 'vm',
    bindings: {}
}
