class FinanceRevenuePaymentController{
    constructor($state){
        'ngInject';

        //
        this.$state = $state;
    }

    $onInit(){
    }

    save_revenue_payment(){
        this.$state.go('app.finance-revenue-payment');
    }
}

export const FinanceRevenuePaymentComponent = {
    templateUrl: './views/app/components/finance-revenue-payment/finance-revenue-payment.component.html',
    controller: FinanceRevenuePaymentController,
    controllerAs: 'vm',
    bindings: {}
}
