class SidebarAdminController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const SidebarAdminComponent = {
    templateUrl: './views/app/components/sidebar-admin/sidebar-admin.component.html',
    controller: SidebarAdminController,
    controllerAs: 'vm',
    bindings: {}
}
