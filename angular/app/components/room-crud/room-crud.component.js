class RoomCrudController{
    constructor(){
        'ngInject';

        //
        angular.element(".room-delete").click(function(){
            if (confirm("Are you sure delete this item?")) {
                alert('Deleting...');
            }
        });
    }

    $onInit(){
    }
}

export const RoomCrudComponent = {
    templateUrl: './views/app/components/room-crud/room-crud.component.html',
    controller: RoomCrudController,
    controllerAs: 'vm',
    bindings: {}
}
