class IssueMainController{
    constructor(API, ToastService, $state, $scope, $window, $http, $q){
        'ngInject';
        this.API = API;
        this.ToastService = ToastService;

        //angular.element('.datepicker').datepicker();
    }

    $onInit(){
        this.status = 0;
        this.issues = this.getIssues();
        console.log(this.issues);
    }
    getIssues(){
        this.ToastService.show('Get OK');
        return this.API.all('issues/get').getList().$object;
    }
}

export const IssueMainComponent = {
    templateUrl: './views/app/components/issue-main/issue-main.component.html',
    controller: IssueMainController,
    controllerAs: 'vm',
    bindings: {}
}