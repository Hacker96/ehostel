class IssueEditController{
    constructor(API, ToastService, $state, $scope, $window, $http, $q){
        'ngInject';
        //
        this.API = API;
        this.$state = $state;
        this.ToastService = ToastService;
        this.$scope = $scope;
        this.$window = $window;
        this.$http = $http;
        this.$q = $q;

        this.guest_issues = {};
                

        this.API = API;
        this.$state = $state;
        this.ToastService = ToastService;


        angular.element(function () {
            angular.element('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
            angular.element(".select2").select2();
        });

        let id = this.$state.params.id;
        this.rooms = this.getRoom();
        this.getDataIssue(id);
   }
    getDataIssue(id){
        let data = this.API.service('get', this.API.all('issues'));

        data.one(id).get().then((response) => {
            this.guest_issues = response[0];
            this.loadGuests(this.guest_issues.issues_id);
        }, () => {
            this.$state.go('app.issue-main');
        });
        
    }
    getRoom(){
        return this.API.all('zoom/get/').getList().$object;
    }
    getGuest(id){
        return this.API.all('guest/get/room/' + id).getList().$object;
    }
    loadGuests(id){
        this.guestList = this.getGuest(id);
    }
    saveIssue(){
        var fd = new FormData();
        angular.forEach(this.guest_issues, function(value, key) {
            fd.append(key, value);
        });

        this.postIssue(fd);

        this.$state.go('app.issue-main');
        this.ToastService.show('Successful');
    }

    postIssue(fd) {
        var rs = this.$q.defer();
        this.$http.post('api/issues/update/' + this.guest_issues.issues_id, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
            }).success(function(){
                rs.resolve(1);
            })
            .error(function(){
                rs.reject(0)
            });

        return rs.promise;
    }
}

export const IssueEditComponent = {
    templateUrl: './views/app/components/issue-edit/issue-edit.component.html',
    controller: IssueEditController,
    controllerAs: 'vm',
    bindings: {}
}
