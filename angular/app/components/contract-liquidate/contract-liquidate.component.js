class ContractLiquidateController{
    constructor($state){
        'ngInject';

        this.$state = $state;

        //
        //Date picker
        angular.element('.datepicker').datepicker({
            autoclose: true
        });
    }

    $onInit(){
    }

    liquidate_contract() {
        this.$state.go('app.contract-main');
    }
}

export const ContractLiquidateComponent = {
    templateUrl: './views/app/components/contract-liquidate/contract-liquidate.component.html',
    controller: ContractLiquidateController,
    controllerAs: 'vm',
    bindings: {}
}
