class ConfigFormController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const ConfigFormComponent = {
    templateUrl: './views/app/components/config-form/config-form.component.html',
    controller: ConfigFormController,
    controllerAs: 'vm',
    bindings: {}
}
