class RoomMainController{
    constructor(API, ToastService){
        'ngInject';

        //
        this.API = API;
        this.ToastService = ToastService;
    }

    $onInit(){
        this.status = 0;
        this.zones = this.getDataRoom();
        //this.renderCSS();
    }

    //renderCSS() {
    //    angular.element('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    //        checkboxClass: 'icheckbox_minimal-blue',
    //        radioClass: 'iradio_minimal-blue'
    //    });
    //}

    getDataRoom(){
        //this.ToastService.show('Get OK');
        return this.API.all('room/collect').getList().$object;
    }
}

export const RoomMainComponent = {
    templateUrl: './views/app/components/room-main/room-main.component.html',
    controller: RoomMainController,
    controllerAs: 'vm',
    bindings: {}
}
