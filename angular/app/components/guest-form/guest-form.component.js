class GuestFormController{
    constructor(API, ToastService, $state, $scope, $window, $http, $q){
        'ngInject';

        this.API = API;
        this.$state = $state;
        this.ToastService = ToastService;
        this.$scope = $scope;
        this.$window = $window;
        this.$http = $http;
        this.$q = $q;

        // Get params
        let id = this.$state.params.id;

        //Init data
        this.data = {};

        //Get guest info
        this.getDataGuest(id);

        /*Init select2*/
        angular.element(".select2").select2();

        //angular.element(document).ready(function(){
        //    angular.element('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        //        checkboxClass: 'icheckbox_minimal-blue',
        //        radioClass: 'iradio_minimal-blue'
        //    });
        //});

        /*Vihicle - add more*/
        angular.element('.add-vehicle').click(function(){
            angular.element('#add-vehicle').before(
                '<div class="form-group">' +
                    '<div class="col-sm-offset-2 col-sm-4">' +
                        '<input type="text" class="form-control pull-right" placeholder="72-X2 1XXX">' +
                    '</div>' +
                    '<label  class="col-sm-1 control-label">Photo</label>' +
                    '<div class="col-sm-3">' +
                        '<input type="file" class="form-control" accept="image/*" multiple>' +
                    '</div>' +
                '</div>'
            );
        });
    }

    $onInit(){
        this.gender = 0;
    }

    getDataGuest(id){
        let data = this.API.service('form', this.API.all('guest'));

        data.one(id).get().then((response) => {
            this.data = response;
        }, () => {
            this.$state.go('app.guest-main');
        });
    }

    saveDataGuest(){
        var fd = new FormData();
        angular.forEach(this.data.guest, function(value, key) {
            fd.append(key, value);
        });

        this.postData(fd);
        //var rs = this.postData(fd);

        //rs.then(function() {
        //    alert(321);
        //    this.$state.go('app.guest-main');
        //    this.ToastService.show('Successful');
        //}, function() {
        //    alert(654);
        //    this.ToastService.error('Internal Error!');
        //});

        this.$state.go('app.guest-main');
        this.ToastService.show('Successful');
    }

    postData(fd) {
        var rs = this.$q.defer();
        this.$http.post('api/guest/save', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
            }).success(function(){
                rs.resolve(1);
            })
            .error(function(){
                rs.reject(0)
                //Nothing
            });

        return rs.promise;
    }
}

export const GuestFormComponent = {
    templateUrl: './views/app/components/guest-form/guest-form.component.html',
    controller: GuestFormController,
    controllerAs: 'vm',
    bindings: {}
}
