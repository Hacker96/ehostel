class ContractDetailController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const ContractDetailComponent = {
    templateUrl: './views/app/components/contract-detail/contract-detail.component.html',
    controller: ContractDetailController,
    controllerAs: 'vm',
    bindings: {}
}
