class EhostelContractAddController{
    constructor(){
        'ngInject';

        //
        angular.element('.datepicker').datepicker({
            autoclose: true
        });
    }

    $onInit(){
    }
}

export const EhostelContractAddComponent = {
    templateUrl: './views/app/components/ehostel-contract-add/ehostel-contract-add.component.html',
    controller: EhostelContractAddController,
    controllerAs: 'vm',
    bindings: {}
}
