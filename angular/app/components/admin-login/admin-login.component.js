class AdminLoginController{
    constructor($auth, $state, ToastService){
        'ngInject';

        //

        angular.element(function () {
            angular.element('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });

        this.$auth = $auth;
        this.$state = $state;
        this.ToastService = ToastService;

        if ($auth.isAuthenticated()) {
            $state.go('app.home');
        }
    }

    $onInit(){
        this.username = '';
        this.password = '';
    }

    login() {
        let user = {
            email: this.username,
            password: this.password
        };

        this.$auth.login(user)
            .then((response) => {
            this.$auth.setToken(response.data);

            //this.ToastService.show('Logged in successfully.');
            this.$state.go('app.home');
        })
        .catch(this.failedLogin.bind(this));
    }

    failedLogin(response) {
        if (response.status === 422) {
            for (let error in response.data.errors) {
                return this.ToastService.error(response.data.errors[error][0]);
            }
        }
        this.ToastService.error(response.statusText);
    }
}

export const AdminLoginComponent = {
    templateUrl: './views/app/components/admin-login/admin-login.component.html',
    controller: AdminLoginController,
    controllerAs: 'vm',
    bindings: {}
}
