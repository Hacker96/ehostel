class EmployeeRoleController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const EmployeeRoleComponent = {
    templateUrl: './views/app/components/employee-role/employee-role.component.html',
    controller: EmployeeRoleController,
    controllerAs: 'vm',
    bindings: {}
}
