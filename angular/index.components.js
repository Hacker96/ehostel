import {EhostelContractAddComponent} from './app/components/ehostel-contract-add/ehostel-contract-add.component';
import {EhostelContractEditComponent} from './app/components/ehostel-contract-edit/ehostel-contract-edit.component';
import {EhostelContractDetailComponent} from './app/components/ehostel-contract-detail/ehostel-contract-detail.component';
import {EhostelMainComponent} from './app/components/ehostel-main/ehostel-main.component';
import {ConfigFormComponent} from './app/components/config-form/config-form.component';
import {EmployeeEditComponent} from './app/components/employee-edit/employee-edit.component';
import {AddIssueComponent} from './app/components/add-issue/add-issue.component';
import {EmployeeRoleComponent} from './app/components/employee-role/employee-role.component';
import {AddEmployeeComponent} from './app/components/add-employee/add-employee.component';
import {EmployeeComponent} from './app/components/employee/employee.component';
import {DashboardComponent} from './app/components/dashboard/dashboard.component';
import {IssueDetailComponent} from './app/components/issue-detail/issue-detail.component';
import {IssueEditComponent} from './app/components/issue-edit/issue-edit.component';
import {IssueMainComponent} from './app/components/issue-main/issue-main.component';
import {ContractLiquidateComponent} from './app/components/contract-liquidate/contract-liquidate.component';
import {FinanceRevenueDetailComponent} from './app/components/finance-revenue-detail/finance-revenue-detail.component';
import {FinanceRevenuePaymentComponent} from './app/components/finance-revenue-payment/finance-revenue-payment.component';
import {FinanceCostFormComponent} from './app/components/finance-cost-form/finance-cost-form.component';
import {FinanceRevenueFormComponent} from './app/components/finance-revenue-form/finance-revenue-form.component';
import {AdminLogoutComponent} from './app/components/admin-logout/admin-logout.component';
import {AdminLoginComponent} from './app/components/admin-login/admin-login.component';
import {FooterAdminComponent} from './app/components/footer-admin/footer-admin.component';
import {HeaderAdminComponent} from './app/components/header-admin/header-admin.component';
import {GuestFormComponent} from './app/components/guest-form/guest-form.component';
import {GuestDetailComponent} from './app/components/guest-detail/guest-detail.component';
import {GuestMainComponent} from './app/components/guest-main/guest-main.component';
import {ContractFormComponent} from './app/components/contract-form/contract-form.component';
import {FinanceCostComponent} from './app/components/finance-cost/finance-cost.component';
import {FinanceRevenueComponent} from './app/components/finance-revenue/finance-revenue.component';
import {RoomDetailComponent} from './app/components/room-detail/room-detail.component';
import {RoomCrudComponent} from './app/components/room-crud/room-crud.component';
import {RoomMainComponent} from './app/components/room-main/room-main.component';
import {ContractDetailComponent} from './app/components/contract-detail/contract-detail.component';
import {ContractMainComponent} from './app/components/contract-main/contract-main.component';
import {SidebarAdminComponent} from './app/components/sidebar-admin/sidebar-admin.component';
import {CreatePostFormComponent} from './app/components/create_post_form/create_post_form.component';
import {AppHeaderComponent} from './app/components/app-header/app-header.component';
import {AppViewComponent} from './app/components/app-view/app-view.component';
import {AppShellComponent} from './app/components/app-shell/app-shell.component';
import {ResetPasswordComponent} from './app/components/reset-password/reset-password.component';
import {ForgotPasswordComponent} from './app/components/forgot-password/forgot-password.component';
import {LoginFormComponent} from './app/components/login-form/login-form.component';
import {RegisterFormComponent} from './app/components/register-form/register-form.component';

angular.module('app.components')
	.component('ehostelContractAdd', EhostelContractAddComponent)
	.component('ehostelContractEdit', EhostelContractEditComponent)
	.component('ehostelContractDetail', EhostelContractDetailComponent)
	.component('ehostelMain', EhostelMainComponent)
	.component('configForm', ConfigFormComponent)
	.component('employeeEdit', EmployeeEditComponent)
	.component('addIssue', AddIssueComponent)
	.component('employeeRole', EmployeeRoleComponent)
	.component('addEmployee', AddEmployeeComponent)
	.component('employee', EmployeeComponent)
	.component('dashboard', DashboardComponent)
	.component('issueEdit', IssueEditComponent)
	.component('issueDetail', IssueDetailComponent)
	.component('issueMain', IssueMainComponent)
	.component('contractLiquidate', ContractLiquidateComponent)
	.component('financeRevenueDetail', FinanceRevenueDetailComponent)
	.component('financeRevenuePayment', FinanceRevenuePaymentComponent)
	.component('financeCostForm', FinanceCostFormComponent)
	.component('financeRevenueForm', FinanceRevenueFormComponent)
	.component('adminLogout', AdminLogoutComponent)
	.component('adminLogin', AdminLoginComponent)
	.component('footerAdmin', FooterAdminComponent)
	.component('headerAdmin', HeaderAdminComponent)
	.component('guestForm', GuestFormComponent)
	.component('guestDetail', GuestDetailComponent)
	.component('guestMain', GuestMainComponent)
	.component('contractForm', ContractFormComponent)
	.component('financeCost', FinanceCostComponent)
	.component('financeRevenue', FinanceRevenueComponent)
	.component('roomDetail', RoomDetailComponent)
	.component('roomCrud', RoomCrudComponent)
	.component('roomMain', RoomMainComponent)
	.component('contractDetail', ContractDetailComponent)
	.component('contractMain', ContractMainComponent)
	.component('sidebarAdmin', SidebarAdminComponent)
	.component('createPostForm', CreatePostFormComponent)
	.component('appHeader', AppHeaderComponent)
	.component('appView', AppViewComponent)
	.component('appShell', AppShellComponent)
	.component('resetPassword', ResetPasswordComponent)
	.component('forgotPassword', ForgotPasswordComponent)
	.component('loginForm', LoginFormComponent)
	.component('registerForm', RegisterFormComponent);

