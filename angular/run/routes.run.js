export function RoutesRun($rootScope, $state, $auth, $transitions) {
    'ngInject';

    let requiresAuthCriteria = {
        to: ($state) => $state.data && $state.data.auth
    };


    let redirectToLogin = () => {
        'ngInject';
        if (!$auth.isAuthenticated()) {
            return $state.target('app.login', undefined, {location: false});
        }
    };

    $transitions.onBefore(requiresAuthCriteria, redirectToLogin, {priority:10});

    angular.element('body').attr('class',$state.get('app').data.bodyClass);

    $rootScope.$on('$viewContentLoaded', function(event){  //toState, toParams, fromState, fromParams
        event.preventDefault();
        // transitionTo() promise will be rejected with
        // a 'transition prevented' error
        if ($state.current.hasOwnProperty('data')) {
            angular.element('body').attr('class', $state.current.data.bodyClass);
        }

        ///*Init app.js*/
        //angular.element(document).ready(function(){
        //    angular.element.AdminLTE.layout.fix();
        //    angular.element.AdminLTE.layout.fixSidebar();
        //});
        ///*Init app.js - END*/
    });
}
