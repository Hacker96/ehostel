
import {FileModelDirective} from './directives/fileModel/fileModel.directive';

angular.module('app.directives')
	.directive('fileModel', ['$parse', FileModelDirective])
	.directive('myRepeatDirective', function() {
	  	return function(scope, element, attrs) {
	  		if (scope.$last){
	    		$('#issues_table').DataTable({"scrollX": true});
	    	}
	  	};
	});
