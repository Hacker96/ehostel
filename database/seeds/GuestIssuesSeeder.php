<?php

use Illuminate\Database\Seeder;

class GuestIssuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 0; $i < 30; $i++){
	        DB::table('guest_issues')->insert([
	        	[
	        		'guest_id' 			=> 1,
	        		'issue_date' 		=> $i . '/10/2016',
	        		'content' 			=> 'The grade power surfaces.',
	        		'cre_func_id'		=> '0',
	        		'cre_user_id'		=> 0,
	        		'mod_func_id'		=> '0',
	        		'mod_user_id'		=> 1
	        	],
	        	[
	        		'guest_id' 			=> 1,
	        		'issue_date' 		=> $i . '/10/2016',
	        		'content' 			=> 'The variance riots.',
	        		'cre_func_id'		=> '0',
	        		'cre_user_id'		=> 0,
	        		'mod_func_id'		=> '0',
	        		'mod_user_id'		=> 1
	        	]
	    	]);
	    }
    }
}
