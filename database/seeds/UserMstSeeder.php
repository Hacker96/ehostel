<?php

use Illuminate\Database\Seeder;

class UserMstSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_mst')->insert([
        	[
        		'account_id'                  =>        1,
				'user_type'                   =>        '01',
				'username'                    =>        'admin',
				'password'                    =>        '$2a$04$TIPutFfavgjEmvjxPqIz7.I91bYsXmElQzoYSNpVcl5TiUml8is92',
                'email'                       =>        'admin',
				'salt'                        =>        '1234567890',
				'status'                      =>        '01',
				'login_fail_counter'          =>        0,
				'cre_func_id'                 =>        '00',
				'cre_user_id'                 =>        0,
				'mod_func_id'                 =>        '00',
				'mod_user_id'                 =>        0
        	]

        ]);
    }
}
