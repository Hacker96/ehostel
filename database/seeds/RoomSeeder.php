<?php

use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 0; $i <= 10; $i++)
        DB::table('room')->insert([
        	[
        		'zone_id'			=> 1,
        		'account_id' 		=> 1,
        		'room_name'			=> 'Room 10.' . $i,
        		'effective_date'	=> '03/10/2016',
        		'price'				=> '10000000',
        		
        		'cre_func_id'		=> '0',
        		'cre_user_id'		=> 0,
        		'mod_func_id'		=> '0',
        		'mod_user_id'		=> 1
        	]
    	]);
    }
}
