<?php

use Illuminate\Database\Seeder;

class CdMstSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('cd_mst')->insert([
        	[
	        	'cd_key' 			=>	'account_type',
	        	'cd'				=>	'01',
	        	'disp_order'		=>	'1'
        	],
        	[
	        	'cd_key' 			=>	'account_type',
	        	'cd'				=>	'02',
	        	'disp_order'		=>	'2'
        	],
        	[
	        	'cd_key' 			=>	'account_status',
	        	'cd'				=>	'00',
	        	'disp_order'		=>	'1'
        	],
        	[
	        	'cd_key' 			=>	'account_status',
	        	'cd'				=>	'01',
	        	'disp_order'		=>	'2'
        	],
        	[
	        	'cd_key' 			=>	'account_status',
	        	'cd'				=>	'02',
	        	'disp_order'		=>	'3'
        	],
        	[
	        	'cd_key' 			=>	'sex_type',
	        	'cd'				=>	'01',
	        	'disp_order'		=>	'1'
        	],
        	[
	        	'cd_key' 			=>	'sex_type',
	        	'cd'				=>	'02',
	        	'disp_order'		=>	'2'
        	],
        	[
	        	'cd_key' 			=>	'sex_type',
	        	'cd'				=>	'03',
	        	'disp_order'		=>	'3'
        	],
        	[
	        	'cd_key' 			=>	'user_type',
	        	'cd'				=>	'01',
	        	'disp_order'		=>	'1'
        	],
        	[
	        	'cd_key' 			=>	'user_type',
	        	'cd'				=>	'02',
	        	'disp_order'		=>	'2'
        	],
        	[
	        	'cd_key' 			=>	'file_cd',
	        	'cd'				=>	'01',
	        	'disp_order'		=>	'1'
        	],
        	[
	        	'cd_key' 			=>	'file_cd',
	        	'cd'				=>	'02',
	        	'disp_order'		=>	'2'
        	],
        	[
	        	'cd_key' 			=>	'file_cd',
	        	'cd'				=>	'03',
	        	'disp_order'		=>	'3'
        	],
        	[
	        	'cd_key' 			=>	'file_cd',
	        	'cd'				=>	'04',
	        	'disp_order'		=>	'4'
        	],
        	[
	        	'cd_key' 			=>	'file_cd',
	        	'cd'				=>	'05',
	        	'disp_order'		=>	'5'
        	],
        	[
	        	'cd_key' 			=>	'file_cd',
	        	'cd'				=>	'06',
	        	'disp_order'		=>	'6'
        	],
        	[
	        	'cd_key' 			=>	'user_status',
	        	'cd'				=>	'01',
	        	'disp_order'		=>	'0'
        	],
        	[
	        	'cd_key' 			=>	'user_status',
	        	'cd'				=>	'02',
	        	'disp_order'		=>	'0'
        	],
        	[
	        	'cd_key' 			=>	'user_status',
	        	'cd'				=>	'00',
	        	'disp_order'		=>	'0'
        	],

        ]);
    }
}