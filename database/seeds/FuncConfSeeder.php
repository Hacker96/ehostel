<?php

use Illuminate\Database\Seeder;

class FuncConfSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('func_conf')->insert([
        	[
	        	'func_conf_key'  	=> 'account_contract_expired_day', 
				'account_id'  		=> 0, 
				'user_id'  			=> 0, 
				'row_no'  			=> 1, 
				'idx_arr'  			=> 0
        	],
        	[
	        	'func_conf_key'  	=> 'electric_price', 
				'account_id'  		=> 0, 
				'user_id'  			=> 0, 
				'row_no'  			=> 1, 
				'idx_arr'  			=> 0
        	],
        	[
	        	'func_conf_key'  	=> 'water_price', 
				'account_id'  		=> 0, 
				'user_id'  			=> 0, 
				'row_no'  			=> 1, 
				'idx_arr'  			=> 0
        	],
        	[
	        	'func_conf_key'  	=> 'day_display_format', 
				'account_id'  		=> 0, 
				'user_id'  			=> 0, 
				'row_no'  			=> 1, 
				'idx_arr'  			=> 0
        	],
        	[
	        	'func_conf_key'  	=> 'month_year_display_format', 
				'account_id'  		=> 0, 
				'user_id'  			=> 0, 
				'row_no'  			=> 1, 
				'idx_arr'  			=> 0
        	]

        ]);
    }
}