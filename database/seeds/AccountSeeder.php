<?php

use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //        
        DB::table('account')->insert([
        	[
		        'account_name'                =>    'eHostel',
		        'account_type'                =>    '01',
		        'cre_func_id'                 =>    '00',
		        'cre_user_id'                 =>     0,
		        'mod_func_id'                 =>    '00',
		        'mod_user_id'                 =>     0
        	],
        ]);
    }
}
