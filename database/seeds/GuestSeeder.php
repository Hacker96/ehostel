<?php

use Illuminate\Database\Seeder;

class GuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 0; $i < 15; $i++)
        DB::table('guest')->insert([
            [
                'room_id'       => $i % 5 + 1,
                'first_name'        => 'Linh',
                'last_name'         => 'Tran Thi Thuy',
                'cre_func_id'       => '0',
                'cre_user_id'       => 0,
                'mod_func_id'       => '0',
                'mod_user_id'       => 1
            ],
        	[
        		'room_id' 		=> $i % 5 + 1,
        		'first_name'		=> 'Trang',
        		'last_name'			=> 'Nguyen Viet',
        		'cre_func_id'		=> '0',
        		'cre_user_id'		=> 0,
        		'mod_func_id'		=> '0',
        		'mod_user_id'		=> 1
        	],
            [
                'room_id'       => $i % 5 + 1,
                'first_name'        => 'Thi',
                'last_name'         => 'Nguyen Dinh',
                'cre_func_id'       => '0',
                'cre_user_id'       => 0,
                'mod_func_id'       => '0',
                'mod_user_id'       => 1
            ]
    	]);
    }
}
