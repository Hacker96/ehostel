<?php

use Illuminate\Database\Seeder;

class RoomZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('room_zone')->insert([
        	[
        		'account_id' 		=> 1,
        		'cre_func_id'		=> '0',
        		'cre_user_id'		=> 0,
        		'mod_func_id'		=> '0',
        		'mod_user_id'		=> 1
        	]
    	]);
    }
}
