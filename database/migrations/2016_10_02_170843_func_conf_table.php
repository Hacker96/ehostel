<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FuncConfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('func_conf', function (Blueprint $table) {
            $table->string("func_conf_key", 100);
            $table->bigInteger("account_id");
            $table->bigInteger("user_id");
            $table->integer("row_no");
            $table->integer("idx_arr");
            $table->string("context", 1000);
            $table->string("cre_func_id", 10);
            $table->timestamp("cre_ts")->useCurrent();
            $table->bigInteger("cre_user_id");
            $table->integer("mod_func_id");
            $table->timestamp("mod_ts")->useCurrent();
            $table->bigInteger("mod_user_id");
            $table->bigInteger("version_no");
            $table->char("del_flg")->default('0');
            $table->primary(['func_conf_key', 'account_id', 'user_id', 'row_no', 'idx_arr'], 'func_conf_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('func_conf');
    }
}