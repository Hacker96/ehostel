<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GuestIssuesTable extends Migration
{
      public function up()
    {
        Schema::create('guest_issues', function (Blueprint $table) {
            $table->bigIncrements('issues_id');
            $table->bigInteger('guest_id')->unsigned();
            $table->date('issue_date')->nullable();
            $table->string('content', 1000)->nullable();

            $table->string('cre_func_id', 10);
            $table->timestamp('cre_ts')->useCurrent();
            $table->timestamp('mod_ts')->useCurrent();
            $table->integer('cre_user_id');
            $table->string('mod_func_id', 10);
            $table->integer('mod_user_id');
            $table->integer('version_no')->default(1);
            $table->char('del_flg', 1)->default('0');
        });
        Schema::table('guest_issues', function ($table) {
            $table->foreign('guest_id')->references('guest_id')->on('guest')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('guest_issues');
    }
}
