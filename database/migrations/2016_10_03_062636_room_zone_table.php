<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoomZoneTable extends Migration
{
    public function up()
    {
        Schema::create('room_zone', function (Blueprint $table) {
            $table->bigIncrements('zone_id');
            $table->bigInteger('account_id')->unsigned();
            $table->string('zone_name', 500)->nullable();
            $table->string('address', 500)->nullable();
            $table->timestamp('cre_ts')->useCurrent();

            $table->string('cre_func_id', 10);
            $table->integer('cre_user_id');
            $table->string('mod_func_id', 10);
            $table->integer('mod_user_id');
            $table->integer('version_no')->default(1);
            $table->char('del_flg', 1)->default('0');
        });
        Schema::table('room_zone', function ($table) {
            $table->foreign('account_id')->references('account_id')->on('account')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('room_zone');
    }
}
