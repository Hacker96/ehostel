<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CdMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cd_mst', function (Blueprint $table) {
            $table->string('cd_key', 100);
            $table->string('cd', 4);
            $table->string('disp_order', 100);
            $table->string('ext_val', 10);
            $table->string('cre_func_id');
            $table->string('cre_ts')->useCurrent();
            $table->bigInteger('cre_user_id');
            $table->string('mod_func_id', 10);
            $table->string('mod_ts')->useCurrent();
            $table->bigInteger('mod_user_id');
            $table->bigInteger('version_no');
            $table->char('del_flg')->default('0');
            $table->primary(['cd_key', 'cd'], 'cd_mst_pri');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cd_mst');
    }
}