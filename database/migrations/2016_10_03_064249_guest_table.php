<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GuestTable extends Migration
{
     public function up()
    {
        Schema::create('guest', function (Blueprint $table) {
            $table->bigIncrements('guest_id');
            $table->bigInteger('room_id')->unsigned();
            $table->string('first_name', 200);
            $table->string('last_name', 200);
            $table->string('phone', 20)->nullable();
            $table->char('char', 1)->nullable();
            $table->string('address', 500)->nullable();
            $table->date('birthday')->nullable();
            $table->string('job_name', 100)->nullable();

            $table->char('residential_reg_flg')->default('0');
            $table->char('room_master_flg')->default('0');
            $table->timestamp('cre_ts')->useCurrent();
            $table->string('cre_func_id', 10);
            $table->integer('cre_user_id');
            $table->string('mod_func_id', 10);
            $table->timestamp('mod_ts')->useCurrent();
            $table->integer('mod_user_id');
            $table->integer('version_no')->default(1);
            $table->char('del_flg', 1)->default('0');
        });
        DB::statement('ALTER TABLE `guest` ADD `identity_card_no` VARBINARY(20)');
        Schema::table('guest', function ($table) {
            $table->foreign('room_id')->references('room_id')->on('room')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('guest');
    }
}
