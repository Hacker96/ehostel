<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoomTable extends Migration
{
     public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->bigIncrements('room_id');
            $table->bigInteger('zone_id')->unsigned();
            $table->bigInteger('account_id')->unsigned();
            $table->string('room_name', 200);
            $table->date('effective_date');
            $table->decimal('price');
            $table->string('cre_func_id', 10);
            $table->timestamp('cre_ts')->useCurrent();
            $table->timestamp('mod_ts')->useCurrent();
            $table->integer('cre_user_id');
            $table->string('mod_func_id', 10);
            $table->integer('mod_user_id');
            $table->integer('version_no')->default(1);
            $table->char('del_flg', 1)->default('0');
        });
        Schema::table('room', function ($table) {
            $table->foreign('zone_id')->references('zone_id')->on('room_zone')->onDelete('cascade');
            $table->foreign('account_id')->references('account_id')->on('account')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('room');
    }
}
