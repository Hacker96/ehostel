<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserMstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_mst', function (Blueprint $table) {
            $table->bigIncrements('user_id');
            $table->bigInteger('account_id')->unsigned();
            $table->string('user_type', 2);
            $table->string('username', 200);
            $table->string('password', 200);
            $table->string('salt', 16);
            $table->string('first_name', 200)->nullable();
            $table->string('last_name', 200)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('email', 200)->nullable();
            $table->string('address', 500)->nullable();
            $table->string('status', 2);
            $table->string('reg_date')->useCurrent();
            $table->integer('login_fail_counter');

            $table->string('cre_func_id', 10);
            $table->integer('cre_user_id');
            $table->string('mod_func_id', 10);
            $table->integer('mod_user_id');
            $table->integer('version_no')->default(1);
            $table->char('del_flg', 1)->default('0');
        });
        Schema::table('user_mst', function ($table) {
            $table->foreign('account_id')->references('account_id')->on('account')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('user_mst');
    }
}
