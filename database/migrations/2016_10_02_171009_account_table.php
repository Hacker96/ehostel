<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('account', function (Blueprint $table) {
            $table->bigIncrements('account_id');
            $table->string('account_name', 200);
            $table->string('phone', 20)->nullable();
            $table->string('address', 500)->nullable();
            $table->string('account_type', 2);
            $table->string('cre_func_id', 10);
            $table->integer('cre_user_id');
            $table->string('mod_func_id', 10);
            $table->integer('mod_user_id');
            $table->integer('version_no')->default(1);
            $table->char('del_flg', 1)->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('account');
    }
}