<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'UPLOAD_FILE' => [
        'PATH' => './storage',
        'ENV' => '/development',
        'ROOT_GUEST_AVATAR' => '/guest/avatar',
        'ROOT_CONTRACT' => '/contract',
        'ROOT_ISSUE' => '/issue',
        'ROOT_LOGO' => '/logo',
        'ROOT_ROOM' => '/room',
        'ROOT_GUEST_MOTORCYCLE' => '/guest/motorcycle',
        'ROOT_GUEST_ATTACH' => '/guest/attach',
    ],

    'FILE_CD' => [
        'GUEST_AVATAR' => '01',
        'CONTRACT' => '02',
        'ISSUE' => '03',
        'LOGO' => '04',
        'ROOM' => '05',
        'GUEST_MOTORCYCLE' => '06',
        'GUEST_ATTACH' => '07',
    ],

];
