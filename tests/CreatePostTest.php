<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreatePostTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
//    public function testExample()
//    {
//        $this->assertTrue(true);
//    }

    public function testStoresPostSuccessfully()
    {

        $post = factory(App\Post::class)->make();

        $this->post('/api/posts', [
            'name' => $post->name,
            'topic' => $post->topic,
        ])
            ->seeApiSuccess()
            ->seeJsonObject('post')
            ->seeJsonKey('name', $post->name)
            ->seeJsonKey('topic', $post->topic);

        $this->seeInDatabase('posts', [
            'name' => $post->name,
            'topic' => $post->topic,
        ]);
    }
}
